#include <stdlib.h>
#include <stdio.h>
#include <time.h>	/* chronometrage */
#include <string.h>     /* pour memset */
#include <math.h>
#include <sys/time.h>
#include <mpi.h>

#include "rasterfile.h"
#include "mandel_lib.h"

#define WORK    1
#define END     2

int rank, nproc;
int master;
int bloc_size;
MPI_Status status;

/* Domaine de calcul dans le plan complexe */
double xmin, ymin;
double xmax, ymax;
/* Dimension de l'image */
int w,h;
/* Pas d'incrementation */
double xinc, yinc;
/* Profondeur d'iteration */
int prof;
/* Image resultat */
unsigned char	*ima, *pima;
/* Variables intermediaires */
int  i, j;
double x, y;

/* Chronometrage */
double debut, fin;

char info[] = "\
        Usage:\n\
        mandel dimx dimy xmin ymin xmax ymax prof\n\
        \n\
        dimx,dimy : dimensions de l'image a generer\n\
                    xmin,ymin,xmax,ymax : domaine a calculer dans le plan complexe\n\
                    prof : nombre maximale d'iteration\n\
                    \n\
                    Quelques exemples d'execution\n\
                    mandel 800 800 0.35 0.355 0.353 0.358 200\n\
                    mandel 800 800 -0.736 -0.184 -0.735 -0.183 500\n\
                    mandel 800 800 -0.736 -0.184 -0.735 -0.183 300\n\
                    mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100\n\
                    mandel 800 800 -1.5 -0.1 -1.3 0.1 10000\n\
                    ";

                    /*
                     * Partie principale: en chaque point de la grille, appliquer xy2color
                     */

void f_master()
{
    int next=rank;
    int i;
    int nb_total = h/bloc_size;
    int* last = (int*)malloc(sizeof(int)*(nproc-2));
    pima = ima = (unsigned char *)malloc( w*h*sizeof(unsigned char));
    if( ima == NULL) {
        fprintf( stderr, "Erreur allocation m�moire du tableau \n");
        return 0;
    }

    /* debut du chronometrage */
    debut = my_gettimeofday();

    for(i = 0; i < nproc-1; i++)
        last[i] = i;

    while(next < nb_total){
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(ima+last[status.MPI_SOURCE]*bloc_size*w, bloc_size*w, MPI_CHAR, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
        last[status.MPI_SOURCE] = next;
        MPI_Send(&next, 1, MPI_INT, status.MPI_SOURCE, WORK, MPI_COMM_WORLD);
        next ++;
    }

    for(i = 0; i < rank; i++)
    {
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(ima+last[status.MPI_SOURCE]*bloc_size*w, bloc_size*w, MPI_CHAR, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, &status);
        MPI_Ssend(&next, 1, MPI_INT, status.MPI_SOURCE, END, MPI_COMM_WORLD);
    }

    /* fin du chronometrage */
    fin = my_gettimeofday();

    sauver_rasterfile( "mandel.ras", w, h, ima);

    printf("Last ..");
    free(last);
    printf("free'd\n");
}

void f_slave()
{
    int send_size;
    int tmp_rank=rank;

    send_size = w*bloc_size;
    /* Allocation memoire du tableau resultat */
    pima = ima = (unsigned char *)malloc( send_size*sizeof(unsigned char));


    if( ima == NULL) {
        fprintf( stderr, "Erreur allocation m�moire du tableau \n");
        return 0;
    }

    /* debut du chronometrage */
    debut = my_gettimeofday();
    do
    {
        pima = ima;
        /* Traitement de la grille point par point */
        y = ymin + (bloc_size * tmp_rank * yinc);

        /* L'algo */
        for (i = 0; i < bloc_size; i++) {
            x = xmin;
            for (j = 0; j < w; j++) {
                *pima++ = xy2color( x, y, prof);
                x += xinc;
            }
            y += yinc;
        }

        MPI_Ssend(ima, send_size, MPI_CHAR, master, END, MPI_COMM_WORLD);

        MPI_Recv(&tmp_rank, 1, MPI_INT, master, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

    }while(status.MPI_TAG != END);
    /* fin du chronometrage */
    fin = my_gettimeofday();
}

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    if( argc == 1) fprintf( stderr, "%s\n", info);

    /* Valeurs par defaut de la fractale */
    xmin = -2; ymin = -2;
    xmax =  2; ymax =  2;
    w = h = 800;
    prof = 10000;

    /* Recuperation des parametres */
    if( argc > 1) w    = atoi(argv[1]);
    if( argc > 2) h    = atoi(argv[2]);
    if( argc > 3) xmin = atof(argv[3]);
    if( argc > 4) ymin = atof(argv[4]);
    if( argc > 5) xmax = atof(argv[5]);
    if( argc > 6) ymax = atof(argv[6]);
    if( argc > 7) prof = atoi(argv[7]);

    /* Calcul des pas d'incrementation */
    xinc = (xmax - xmin) / (w-1);
    yinc = (ymax - ymin) / (h-1);

    /* affichage parametres pour verificatrion */
    fprintf( stderr, "Domaine: {[%lg,%lg]x[%lg,%lg]}\n", xmin, ymin, xmax, ymax);
    fprintf( stderr, "Increment : %lg %lg\n", xinc, yinc);
    fprintf( stderr, "Prof: %d\n",  prof);
    fprintf( stderr, "Dim image: %dx%d\n", w, h);

    bloc_size = 10;
    master = nproc-1;
    if(rank == master)
        f_master();
    else
        f_slave();

    fprintf( stderr, "[%d] Temps total de calcul : %g sec\n", rank,
             fin - debut);
    fprintf( stdout, "[%d] %g\n", rank, fin - debut);

    printf("Ima ..");
    free(ima);
    printf("free'd\n");

    MPI_Finalize();

    return 0;
}

