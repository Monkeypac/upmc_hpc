#include "mandel_lib.h"


                    double my_gettimeofday(){
    struct timeval tmp_time;
    gettimeofday(&tmp_time, NULL);
    return tmp_time.tv_sec + (tmp_time.tv_usec * 1.0e-6L);
}




/**
 * Convertion entier (4 octets) LINUX en un entier SUN
 * @param i entier � convertir
 * @return entier converti
 */

int swap(int i) {
    int init = i;
    int conv;
    unsigned char *o, *d;

    o = ( (unsigned char *) &init) + 3;
    d = (unsigned char *) &conv;

    *d++ = *o--;
    *d++ = *o--;
    *d++ = *o--;
    *d++ = *o--;

    return conv;
}


/***
 * Par Francois-Xavier MOREL (M2 SAR, oct2009):
 */

unsigned char power_composante(int i, int p) {
    unsigned char o;
    double iD=(double) i;

    iD/=255.0;
    iD=pow(iD,p);
    iD*=255;
    o=(unsigned char) iD;
    return o;
}

unsigned char cos_composante(int i, double freq) {
    unsigned char o;
    double iD=(double) i;
    iD=cos(iD/255.0*2*M_PI*freq);
    iD+=1;
    iD*=128;

    o=(unsigned char) iD;
    return o;
}


/**
 *  Sauvegarde le tableau de donn�es au format rasterfile
 *  8 bits avec une palette de 256 niveaux de gris du blanc (valeur 0)
 *  vers le noir (255)
 *    @param nom Nom de l'image
 *    @param largeur largeur de l'image
 *    @param hauteur hauteur de l'image
 *    @param p pointeur vers tampon contenant l'image
 */

void sauver_rasterfile( char *nom, int largeur, int hauteur, unsigned char *p) {
    FILE *fd;
    struct rasterfile file;
    int i;
    unsigned char o;

    if ( (fd=fopen(nom, "w")) == NULL ) {
        printf("erreur dans la creation du fichier %s \n",nom);
        exit(1);
    }

    file.ras_magic  = swap(RAS_MAGIC);
    file.ras_width  = swap(largeur);	  /* largeur en pixels de l'image */
    file.ras_height = swap(hauteur);         /* hauteur en pixels de l'image */
    file.ras_depth  = swap(8);	          /* profondeur de chaque pixel (1, 8 ou 24 )   */
    file.ras_length = swap(largeur*hauteur); /* taille de l'image en nb de bytes		*/
    file.ras_type    = swap(RT_STANDARD);	  /* type de fichier */
    file.ras_maptype = swap(RMT_EQUAL_RGB);
    file.ras_maplength = swap(256*3);

    fwrite(&file, sizeof(struct rasterfile), 1, fd);

    /* Palette de couleurs : composante rouge */
    i = 256;
    while( i--) {
        o = COMPOSANTE_ROUGE(i);
        fwrite( &o, sizeof(unsigned char), 1, fd);
    }

    /* Palette de couleurs : composante verte */
    i = 256;
    while( i--) {
        o = COMPOSANTE_VERT(i);
        fwrite( &o, sizeof(unsigned char), 1, fd);
    }

    /* Palette de couleurs : composante bleu */
    i = 256;
    while( i--) {
        o = COMPOSANTE_BLEU(i);
        fwrite( &o, sizeof(unsigned char), 1, fd);
    }

    // pour verifier l'ordre des lignes dans l'image :
    //fwrite( p, largeur*hauteur/3, sizeof(unsigned char), fd);

    // pour voir la couleur du '0' :
    // memset (p, 0, largeur*hauteur);

    fwrite( p, largeur*hauteur, sizeof(unsigned char), fd);
    fclose( fd);
}

/**
 * �tant donn�e les coordonn�es d'un point \f$c=a+ib\f$ dans le plan
 * complexe, la fonction retourne la couleur correspondante estimant
 * � quelle distance de l'ensemble de mandelbrot le point est.
 * Soit la suite complexe d�fini par:
 * \f[
 * \left\{\begin{array}{l}
 * z_0 = 0 \\
 * z_{n+1} = z_n^2 - c
 * \end{array}\right.
 * \f]
 * le nombre d'it�rations que la suite met pour diverger est le
 * nombre \f$ n \f$ pour lequel \f$ |z_n| > 2 \f$.
 * Ce nombre est ramen� � une valeur entre 0 et 255 correspond ainsi a
 * une couleur dans la palette des couleurs.
 */

unsigned char xy2color(double a, double b, int prof) {
    double x, y, temp, x2, y2;
    int i;

    x = y = 0.;
    for( i=0; i<prof; i++) {
        /* garder la valeur pr�c�dente de x qui va etre ecrase */
        temp = x;
        /* nouvelles valeurs de x et y */
        x2 = x*x;
        y2 = y*y;
        x = x2 - y2 + a;
        y = 2*temp*y + b;
        if( x2 + y2 >= 4.0) break;
    }
    return (i==prof)?255:(int)((i%255));
}
