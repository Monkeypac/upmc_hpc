#include <stdlib.h>
#include <stdio.h>
#include <time.h>	/* chronometrage */
#include <string.h>     /* pour memset */
#include <math.h>
#include <sys/time.h>
#include <mpi.h>

#include "rasterfile.h"
#include "mandel_lib.h"

char info[] = "\
        Usage:\n\
        mandel dimx dimy xmin ymin xmax ymax prof\n\
        \n\
        dimx,dimy : dimensions de l'image a generer\n\
                    xmin,ymin,xmax,ymax : domaine a calculer dans le plan complexe\n\
                    prof : nombre maximale d'iteration\n\
                    \n\
                    Quelques exemples d'execution\n\
                    mandel 800 800 0.35 0.355 0.353 0.358 200\n\
                    mandel 800 800 -0.736 -0.184 -0.735 -0.183 500\n\
                    mandel 800 800 -0.736 -0.184 -0.735 -0.183 300\n\
                    mandel 800 800 -1.48478 0.00006 -1.48440 0.00044 100\n\
                    mandel 800 800 -1.5 -0.1 -1.3 0.1 10000\n\
                    ";

/*
 * Partie principale: en chaque point de la grille, appliquer xy2color
 */

int main(int argc, char *argv[]) {
    int rank, nproc;
    MPI_Status status;

    /* Domaine de calcul dans le plan complexe */
    double xmin, ymin;
    double xmax, ymax;
    /* Dimension de l'image */
    int w,h;
    /* Pas d'incrementation */
    double xinc, yinc;
    /* Profondeur d'iteration */
    int prof;
    /* Image resultat */
    unsigned char	*ima, *pima;
    unsigned char	*d_ima, *d_pima;
    /* Variables intermediaires */
    int  i, j;
    double x, y;
    /* Chronometrage */
    double debut, fin;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    /* debut du chronometrage */
    debut = my_gettimeofday();


    if( argc == 1) fprintf( stderr, "%s\n", info);

    /* Valeurs par defaut de la fractale */
    xmin = -2; ymin = -2;
    xmax =  2; ymax =  2;
    w = h = 800;
    prof = 10000;

    /* Recuperation des parametres */
    if( argc > 1) w    = atoi(argv[1]);
    if( argc > 2) h    = atoi(argv[2]);
    if( argc > 3) xmin = atof(argv[3]);
    if( argc > 4) ymin = atof(argv[4]);
    if( argc > 5) xmax = atof(argv[5]);
    if( argc > 6) ymax = atof(argv[6]);
    if( argc > 7) prof = atoi(argv[7]);

    /* Calcul des pas d'incrementation */
    xinc = (xmax - xmin) / (w-1);
    yinc = (ymax - ymin) / (h-1);

    /* affichage parametres pour verificatrion */
    fprintf( stderr, "Domaine: {[%lg,%lg]x[%lg,%lg]}\n", xmin, ymin, xmax, ymax);
    fprintf( stderr, "Increment : %lg %lg\n", xinc, yinc);
    fprintf( stderr, "Prof: %d\n",  prof);
    fprintf( stderr, "Dim image: %dx%d\n", w, h);

    /* Allocation memoire du tableau resultat */
    if(!rank) {
        pima = ima = (unsigned char *)malloc( w*h*sizeof(unsigned char));
    }
    else {
        pima = ima = (unsigned char *)malloc( w*(h/2)/nproc*sizeof(unsigned char));
    }

    d_pima = d_ima = (unsigned char *)malloc( w*(h/2)/nproc*sizeof(unsigned char));

    if( ima == NULL) {
        fprintf( stderr, "Erreur allocation m�moire du tableau \n");
        return 0;
    }

    /* Traitement de la grille point par point */
    y = ymin + ((h/2)/nproc * rank * yinc);
    for (i = 0; i < (h/2)/nproc; i++) {
        x = xmin;
        for (j = 0; j < w; j++) {
            *pima++ = xy2color( x, y, prof);
            x += xinc;
        }
        y += yinc;
    }

    y = ymin + ( ( ((h/2)/nproc * rank) + (h/2)) * yinc);
    for (i = 0; i < (h/2)/nproc; i++) {
        x = xmin;
        for (j = 0; j < w; j++) {
            *d_pima++ = xy2color( x, y, prof);
            x += xinc;
        }
        y += yinc;
    }


    MPI_Gather(ima, (h/2)/nproc * w, MPI_CHAR, ima, (h/2)/nproc * w, MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(d_ima, (h/2)/nproc * w, MPI_CHAR, ima+(h/2)*w, (h/2)/nproc * w, MPI_CHAR, 0, MPI_COMM_WORLD);

    /* fin du chronometrage */
    fin = my_gettimeofday();
    fprintf( stderr, "[%d] Temps total de calcul : %g sec\n", rank,
             fin - debut);
    fprintf( stdout, "[%d] %g\n", rank, fin - debut);

    if (!rank)
        /* Sauvegarde de la grille dans le fichier resultat "mandel.ras" */
        sauver_rasterfile( "mandel.ras", w, h, ima);

    MPI_Finalize();

    free(ima);

    return 0;
}
