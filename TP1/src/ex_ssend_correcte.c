#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

int main(int argc, char**argv)
{
    int rank, nproc;
    int dest, src;
    char data[100];

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    dest = (rank+1)%nproc;
    src = (rank-1+nproc)%nproc;

    if(!rank)
    {
        sprintf(data, "Message de %d à %d", rank, dest);
        MPI_Ssend(data, strlen(data) + 1, MPI_CHAR, dest, 1, MPI_COMM_WORLD);
    }

    MPI_Recv(data, 100, MPI_CHAR, src, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    if(rank)
    {
        sprintf(data, "Message de %d à %d", rank, dest);
        MPI_Ssend(data, strlen(data) + 1, MPI_CHAR, dest, 1, MPI_COMM_WORLD);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    printf("[%d] %s\n", rank, data);

    MPI_Finalize();

    return 0;
}
