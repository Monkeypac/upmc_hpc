#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

#define SEND_SIZE 1000

int main(int argc, char**argv)
{
    int rank, nproc;
    int dest, src;
    int i;
    char* data;

    data = calloc(SEND_SIZE, 1);

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    dest = (rank+1)%nproc;
    src = (rank-1+nproc)%nproc;

    MPI_Send(data, SEND_SIZE, MPI_CHAR, dest, 1, MPI_COMM_WORLD);
    fflush(stdout);
    MPI_Recv(data, SEND_SIZE, MPI_CHAR, src, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    MPI_Barrier(MPI_COMM_WORLD);
    printf("[%d] %s\n", rank, data);

    MPI_Finalize();

    free(data);
    return 0;
}

/*
 * 100ko (meme un peu moins selon l'architecture) rend le send bloquant du à une longueur de message trop importante.
 */
