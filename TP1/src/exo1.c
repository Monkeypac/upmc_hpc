#include <stdio.h>
#include <mpi.h>

int main(int argc, char**argv)
{
    int rank, nproc;
    int valeur, tag;
    MPI_Status status;

    tag = 10;

    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(rank == 1)
    {
        valeur = 18;
        MPI_Send(&valeur, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
    }
    else if(!rank)
    {
        MPI_Recv(&valeur, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &status);
        printf("J'ai recu la valeur %d du processus de rang 1.\n", valeur);
    }


    MPI_Finalize();

    return 0;
}

/*
 * Ce programme doit afficher:
 J'ai recu la valeur 18 du processus 1.
 */
