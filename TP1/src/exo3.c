#include <stdio.h>
#include <mpi.h>
#include <time.h>

void fils(int rank, int *a, int *b)
{
    *a = rank*2 +1;
    *b = rank*2 +2;
}

void pere(int rank, int *p)
{
    *p = (rank-1)/2;
}

int main(int argc, char **argv)
{
    int rank, nproc;
    int valeur, i, tmp;
    MPI_Status status;

    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    srand(time(NULL)+rank);
    valeur = rand() % 100;

    printf("[%d] Value: %d\n", rank, valeur);

    int a, b, pere_id;
    fils(rank, &a, &b);
    pere(rank, &pere_id);
    //printf("%d: a%d b%d p%d\n", rank, a, b, pere_id);
    if(a < nproc - 1) //Must receive
    {
        //printf("%d recv %d\n", rank, a);
        MPI_Recv(&tmp, 1, MPI_INT, a, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        valeur += tmp;
        if(b < nproc - 1)
        {
            //printf("%d recv %d\n", rank, b);
            MPI_Recv(&tmp, 1, MPI_INT, b, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            valeur += tmp;
        }

        printf("Sub sum: %d\n", valeur);
    }

    if(rank)
    {
        //printf("%d send %d\n", rank, pere_id);
        MPI_Send(&valeur, 1, MPI_INT, pere_id, 1, MPI_COMM_WORLD);
    }
    else
    {
        printf("Final sum: %d\n", valeur);
    }

    MPI_Finalize();

    return 0;
}
