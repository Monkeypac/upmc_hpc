#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>

int main(int argc, char **argv)
{
    int rank, nproc;
    int valeur, i, tmp;
    MPI_Status status;

    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    srand(time(NULL) + rank);
    valeur = rand() % 100;

    printf("[%d] Value: %d\n", rank, valeur);

    //MPI_Reduce(&valeur, &tmp, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Allreduce(&valeur, &tmp, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

    //if(!rank)
        printf("[%d] Final sum: %d\n", rank, tmp);

    MPI_Finalize();

    return 0;
}
